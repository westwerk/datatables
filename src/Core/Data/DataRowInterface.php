<?php

namespace Westwerk\DataTables\Core\Data;

/**
 * Class DataRowInterface
 *
 * @package Westwerk\DataTablesBundle\Core\DataRowInterface
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
interface DataRowInterface {

    /**
     * @param DataColumnInterface $column
     * @return mixed
     */
    public function addColumn(DataColumnInterface $column);

    /**
     * @return DataColumnInterface
     */
    public function getNext();

    /**
     * @return void
     */
    public function reset();

    /**
     * @return array
     */
    public function toArray();

}