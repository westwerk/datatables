<?php

namespace Westwerk\DataTables\Core\Data;
use Westwerk\DataTables\Core\Column\ColumnInterface;

/**
 * Class DataColumnInterface
 *
 * @package Westwerk\DataTablesBundle\Core\DataColumnInterface
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
interface DataColumnInterface {

    /**
     * @return mixed
     */
    public function getValue();

    /**
     * @return ColumnInterface
     */
    public function getDefinition();

    /**
     * @return mixed
     */
    public function getExportValue();

}