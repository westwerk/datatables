<?php

namespace Westwerk\DataTables\Core\Data;

use Westwerk\DataTables\Core\Column\ColumnInterface;

/**
 * Class AbstractDataColumn
 *
 * @package Westwerk\DataTablesBundle\Core\AbstractDataColumn
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
abstract class AbstractDataColumn implements DataColumnInterface {

    /**
     * @var ColumnInterface
     */
    protected $definition;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @var mixed
     */
    protected $exportValue;

    /**
     * @param ColumnInterface $def
     */
    public function __construct(ColumnInterface $def) {
        $this->setDefinition($def);
    }

    /**
     * @return ColumnInterface
     */
    public function getDefinition()
    {
        return $this->definition;
    }

    /**
     * @param ColumnInterface $def
     */
    protected function setDefinition($def)
    {
        $this->definition = $def;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    protected function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getExportValue()
    {
        return $this->exportValue;
    }

    /**
     * @param $exportValue
     */
    protected function setExportValue($exportValue)
    {
        $this->exportValue = $exportValue;
    }

}