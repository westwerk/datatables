<?php

namespace Westwerk\DataTables\Core\Data;

use Westwerk\DataTables\Core\Data\DataColumnInterface;
use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;

/**
 * Class AbstractDataRow
 *
 * @package Westwerk\DataTablesBundle\Core\AbstractDataRow
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
abstract class AbstractDataRow implements DataRowInterface {

    /**
     * @var DataColumnInterface[]
     */
    protected $columns = array();

    /**
     * @var ColumnBuilderInterface
     */
    protected $columnBuilder;

    /**
     * @param ColumnBuilderInterface $columnBuilder
     */
    public function __construct(ColumnBuilderInterface $columnBuilder) {
        $this->setColumnBuilder($columnBuilder);
    }

    /**
     * @return array
     */
    public function toArray() {
        $data = array();
        while (($column = $this->getNext()) && $column !== false) {
            if (!$column->getDefinition()->isHidden()) {
                $data[] = $column->getValue();
            }
        }
        return $data;
    }

    /**
     * @param DataColumnInterface $column
     * @return $this
     */
    public function addColumn(DataColumnInterface $column) {
        $this->columns[] = $column;
        return $this;
    }

    /**
     * @return DataColumnInterface
     */
    public function getNext() {
        $column = current($this->columns);
        if ($column !== false) {
            next($this->columns);
            return $column;
        }
        return false;
    }

    public function reset() {
       reset($this->columns);
        return $this;
    }

    /**
     * @return ColumnBuilderInterface
     */
    public function getColumnBuilder()
    {
        return $this->columnBuilder;
    }

    /**
     * @param ColumnBuilderInterface $columnBuilder
     */
    public function setColumnBuilder($columnBuilder)
    {
        $this->columnBuilder = $columnBuilder;
    }




}