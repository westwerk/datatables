<?php

namespace Westwerk\DataTables\Core\ColumnBuilder;

use Westwerk\DataTables\Core\Column\ColumnInterface;

/**
 * Class ColumnBuilderInterface
 *
 * @package Westwerk\DataTablesBundle\Core\ColumnBuilder
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
interface ColumnBuilderInterface
{

    /**
     * Add a column interface
     *
     * @param ColumnInterface $column
     * @return $this
     */
    public function add(ColumnInterface $column);

    /**
     * Delete a column by name or index.
     *
     * @param $name string|int
     * @return $this
     */
    public function remove($name);

    /**
     * Get all columns.
     *
     * @return ColumnInterface[]
     */
    public function getColumns();

    /**
     * @return ColumnInterface[]
     */
    public function getVisibleColumns();

    /**
     * @param ColumnInterface $column
     * @param                 $after
     *
     * @return $this|bool
     */
    public function addAfter(ColumnInterface $column, $after);

    /**
     * @param ColumnInterface $column
     * @param                 $before
     *
     * @return $this|bool
     */
    public function addBefore(ColumnInterface $column, $before);

    /**
     * @param $name
     *
     * @return bool|ColumnInterface
     */
    public function getColumn($name);


}