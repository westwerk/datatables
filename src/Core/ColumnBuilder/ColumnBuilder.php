<?php

namespace Westwerk\DataTables\Core\ColumnBuilder;

use Westwerk\DataTables\Core\Column\ColumnInterface;
use Westwerk\DataTables\Core\Column\Column;

/**
 * Class ColumnBuilder
 *
 * @package Westwerk\DataTablesBundle\Core\ColumnBuilder
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
class ColumnBuilder implements ColumnBuilderInterface
{

    /**
     * @var ColumnInterface[]
     */
    protected $columns = array();

    /**
     * Add a column interface
     *
     * @param ColumnInterface $column
     * @return $this
     */
    public function add(ColumnInterface $column) {
        $this->columns[] = $column;
        return $this;
    }

    /**
     * Delete a column by name or index.
     *
     * @param $name string|int
     * @return $this
     */
    public function remove($name) {
        $index = $this->indexOf($name);
        if ($index !== false) {
            unset($this->columns[$index]);
            $this->columns = array_values($this->columns);
        }
        return $this;
    }

    /**
     * @param ColumnInterface $column
     * @param                 $after
     *
     * @return $this|bool
     */
    public function addAfter(ColumnInterface $column, $after) {
        $index = $this->indexOf($after);
        if ($index !== false) {
            $this->insertAfter($column, $index);
        }
        return $this;
    }

    /**
     * @param ColumnInterface $column
     * @param                 $before
     *
     * @return $this|bool
     */
    public function addBefore(ColumnInterface $column, $before) {
        $index = $this->indexOf($before);
        if ($index !== false) {
            $this->insertBefore($column, $index);
        }
        return $this;
    }

    /**
     * Get all columns.
     *
     * @return ColumnInterface[]
     */
    public function getColumns() {
        return $this->columns;
    }

    /**
     * @return ColumnInterface[]
     */
    public function getVisibleColumns() {
       return array_values(array_filter($this->getColumns(), function(ColumnInterface $column) {
          return !$column->isHidden();
       }));
    }

    /**
     * @param $name
     *
     * @return bool|ColumnInterface
     */
    public function getColumn($name) {
        $index = $this->indexOf($name);
        if (array_key_exists($index, $this->columns)) {
            return $this->columns[$index];
        }
        return false;
    }

    /**
     * @param $needle
     *
     * @return bool|int|mixed|string
     */
    protected function indexOf($needle) {
        if ($needle instanceof ColumnInterface) {
            return array_search($needle, $this->columns, true);
        }
        foreach ($this->columns as $index=>$column) {
            if ($column->getName() === $needle) {
                return $index;
            }
        }
        return false;
    }

    /**
     * @param ColumnInterface $column
     * @param                 $index
     */
    protected function insertBefore(ColumnInterface $column, $index) {
        array_splice($this->columns, $index, 0, array($column));
    }

    /**
     * @param ColumnInterface $column
     * @param                 $index
     */
    protected function insertAfter(ColumnInterface $column, $index) {
        array_splice($this->columns, $index + 1, 0, array($column));
    }


}