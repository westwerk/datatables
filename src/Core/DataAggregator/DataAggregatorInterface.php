<?php

namespace Westwerk\DataTables\Core\DataAggregator;

use Westwerk\DataTables\Core\DataCollection\DataCollectionInterface;
use Westwerk\DataTables\Core\DataSource\DataSourceInterface;

/**
 * Class DataAggregatorInterface
 *
 * @package Westwerk\DataTablesBundle\Core\DataAggregator
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
interface DataAggregatorInterface
{

    /**
     * @return array
     */
    public function getData();

    /**
     * @return DataCollectionInterface
     */
    public function getDataCollection();

    /**
     * @param DataSourceInterface $dataSource
     */
    public function setDataSource($dataSource);

    /**
     * @return DataSourceInterface
     */
    public function getDataSource();

}