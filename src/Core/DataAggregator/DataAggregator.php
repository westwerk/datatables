<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 28/02/15
 * Time: 21:55
 */

namespace Westwerk\DataTables\Core\DataAggregator;


use Symfony\Component\HttpFoundation\Request;
use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;
use Westwerk\DataTables\Core\Column\ColumnInterface;
use Westwerk\DataTables\Core\DataCollection\DataCollectionInterface;
use Westwerk\DataTables\Core\DataSource\DataSourceInterface;

class DataAggregator implements DataAggregatorInterface
{

    /**
     * @var ColumnBuilderInterface
     */
    protected $columnBuilder;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var DataSourceInterface
     */
    protected $dataSource;


    public function __construct(ColumnBuilderInterface $columnBuilder, Request $request)
    {
        $this->columnBuilder = $columnBuilder;
        $this->request       = $request;
    }

    /**
     * @return DataCollectionInterface
     */
    public function getDataCollection() {
        $dataSource = $this->getDataSource();
        $dataCollection = $dataSource->getDataCollection();
        return $dataCollection;
    }

    /**
     * @return array
     */
    public function getData()
    {
        $dataSource = $this->getDataSource();
        $dataCollection = $this->getDataCollection();

        return [
            'data' => $dataCollection->toArray(),
            'iTotalRecords' => $dataSource->getTotalRecords(),
            'iTotalDisplayRecords' => $dataSource->getTotalDisplayRecords()
        ];
    }


    /**
     * @param DataSourceInterface $dataSource
     */
    protected function prepareDataSource(DataSourceInterface $dataSource) {
        $columns = $this->getColumnBuilder()->getVisibleColumns();

        // Limits
        $dataSource->setFirstResult(intval($this->request->get('start')));
        $dataSource->setMaxResults(intval($this->request->get('length')));

        // Fetch parameters
        $pOrders = $this->request->get('order', array());
        $pColumns = $this->request->get('columns', array());
        $pSearch = $this->request->get('search', array());

        // Global search
        if (is_array($pSearch)) {
            // Global filter
            if (array_key_exists('value',$pSearch) && strlen($pSearch['value']) > 0) {
                $dataSource->setFilterString($pSearch['value']);
            }
        }


        // Column search
        if (is_array($pColumns)) {
            foreach ($pColumns as $key=>$pColumn) {
                if (array_key_exists($key, $columns)) {
                    /** @var ColumnInterface $column */
                    $column = $columns[$key];
                    if (isset($pColumn['searchable'])
                        && $pColumn['searchable'] == 'true'
                        && array_key_exists('search', $pColumn)
                        && array_key_exists('value', $pColumn['search'])
                        && strlen($pColumn['search']['value']) > 0) {
                        $column->setFilterString($pColumn['search']['value']);
                    }
                }
            }
        }

        // Sorting
        if (is_array($pColumns)) {
            // Sorting
            if (is_array($pOrders)) {
                foreach ($pOrders as $key=>$pOrder) {
                    if (array_key_exists('column', $pOrder)) {
                        $colNo = $pOrder['column'];

                        if (array_key_exists($colNo, $columns)
                            && array_key_exists($colNo, $pColumns)) {
                            /** @var ColumnInterface $column */
                            $column = $columns[$colNo];
                            if ($column->isSortable()
                                && isset($pColumns[$key]['orderable'])
                                && $pColumns[$key]['orderable'] == 'true') {
                                $column->setSorted(true);
                                if (isset($pOrder['dir']) && $pOrder['dir'] == 'desc')
                                    $column->setSortDirection(ColumnInterface::SORT_DIRECTION_DESC);
                                else
                                    $column->setSortDirection(ColumnInterface::SORT_DIRECTION_ASC);
                            }
                        }
                    }
                }
            }
        }

        //Prepare the datasource
        $dataSource->prepare();
    }


    /**
     * @return ColumnBuilderInterface
     */
    public function getColumnBuilder()
    {
        return $this->columnBuilder;
    }

    /**
     * @param ColumnBuilderInterface $columnBuilder
     */
    public function setColumnBuilder($columnBuilder)
    {
        $this->columnBuilder = $columnBuilder;
    }

    /**
     * @return DataSourceInterface
     */
    public function getDataSource()
    {
        return $this->dataSource;
    }

    /**
     * @param DataSourceInterface $dataSource
     */
    public function setDataSource($dataSource)
    {
        $dataSource->setColumnBuilder($this->getColumnBuilder());
        $this->prepareDataSource($dataSource);
        $this->dataSource = $dataSource;
    }



}