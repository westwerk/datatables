<?php

namespace Westwerk\DataTables\Core\DataSource;
use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;
use Westwerk\DataTables\Core\DataCollection\DataCollectionInterface;


abstract class AbstractDataSource implements DataSourceInterface
{

    /**
     * @var null|int
     */
    protected $totalRecords = null;

    /**
     * @var null|int
     */
    protected $totalDisplayRecords = null;

    /**
     * @var null
     */
    protected $firstResult = null;

    /**
     * @var null
     */
    protected $maxResults = null;

    /**
     * @var ColumnBuilderInterface
     */
    protected $columnBuilder = null;

    /**
     * @var DataCollectionInterface
     */
    protected $dataCollection;


    /**
     * @var null|callable
     */
    protected $onCountTotalRecords = null;


    /**
     * @var null|callable
     */
    protected $onCountTotalDisplayRecords = null;


    /**
     * @var null|callable
     */
    protected $onFilter = null;


    /**
     * @var bool
     */
    protected $filtered = false;

    /**
     * @var string
     */
    protected $filterString = '';


    /**
     * @return ColumnBuilderInterface
     */
    public function getColumnBuilder()
    {
        return $this->columnBuilder;
    }


    /**
     * @param ColumnBuilderInterface $columnBuilder
     * @return mixed|void
     */
    public function setColumnBuilder(ColumnBuilderInterface $columnBuilder)
    {
        $this->columnBuilder = $columnBuilder;
    }


    /**
     * @return int
     */
    public function getTotalDisplayRecords()
    {
        return intval($this->totalDisplayRecords);
    }

    /**
     * @return int
     */
    public function getTotalRecords()
    {
        return intval($this->totalRecords);
    }

    /**
     * @param int|null $totalDisplayRecords
     */
    protected function setTotalDisplayRecords($totalDisplayRecords)
    {
        $this->totalDisplayRecords = $totalDisplayRecords;
    }

    /**
     * @param int|null $totalRecords
     */
    protected function setTotalRecords($totalRecords)
    {
        $this->totalRecords = $totalRecords;
    }

    /**
     * @return int
     */
    public function getFirstResult()
    {
        return $this->firstResult;
    }

    /**
     * @param $firstResult
     * @return void
     */
    public function setFirstResult($firstResult)
    {
        $this->firstResult = $firstResult;
    }

    /**
     * @return int
     */
    public function getMaxResults()
    {
        return $this->maxResults;
    }

    /**
     * @param $maxResults
     * @return void
     */
    public function setMaxResults($maxResults)
    {
        $this->maxResults = $maxResults;
    }

    /**
     * @return DataCollectionInterface
     */
    public function getDataCollection()
    {
        return $this->dataCollection;
    }

    /**
     * @param DataCollectionInterface $dataCollection
     */
    public function setDataCollection($dataCollection)
    {
        $this->dataCollection = $dataCollection;
    }

    /**
     * @return callable|null
     */
    public function getOnCountTotalDisplayRecords()
    {
        return $this->onCountTotalDisplayRecords;
    }

    /**
     * @param callable|null $onCountTotalDisplayRecords
     * @return $this
     */
    public function setOnCountTotalDisplayRecords($onCountTotalDisplayRecords)
    {
        $this->onCountTotalDisplayRecords = $onCountTotalDisplayRecords;
        return $this;
    }

    /**
     * @return callable|null
     */
    public function getOnCountTotalRecords()
    {
        return $this->onCountTotalRecords;
    }

    /**
     * @param callable|null $onCountTotalRecords
     * @return $this
     */
    public function setOnCountTotalRecords($onCountTotalRecords)
    {
        $this->onCountTotalRecords = $onCountTotalRecords;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilterString()
    {
        return $this->filterString;
    }

    /**
     * @param string $filterString
     */
    public function setFilterString($filterString)
    {
        $this->setFiltered((strlen($filterString) > 0));
        $this->filterString = $filterString;
    }

    /**
     * @return boolean
     */
    public function isFiltered()
    {
        return $this->filtered;
    }

    /**
     * @param boolean $filtered
     */
    public function setFiltered($filtered)
    {
        $this->filtered = $filtered;
    }

    /**
     * @return callable|null
     */
    public function getOnFilter()
    {
        return $this->onFilter;
    }

    /**
     * @param callable|null $onFilter
     */
    public function setOnFilter($onFilter)
    {
        $this->onFilter = $onFilter;
    }



}
