<?php

namespace Westwerk\DataTables\Core\DataSource;

use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;
use Westwerk\DataTables\Core\DataCollection\DataCollectionInterface;

interface DataSourceInterface
{

    /**
     * @param ColumnBuilderInterface $columnBuilder
     * @return mixed
     */
    public function setColumnBuilder(ColumnBuilderInterface $columnBuilder);

    /**
     * @return mixed
     */
    public function prepare();

    /**
     * @return int
     */
    public function getTotalRecords();

    /**
     * @return int
     */
    public function getTotalDisplayRecords();

    /**
     * @return DataCollectionInterface
     */
    public function getDataCollection();

    /**
     * @param callable|null $onCountTotalDisplayRecords
     * @return $this
     */
    public function setOnCountTotalDisplayRecords($onCountTotalDisplayRecords);

    /**
     * @param callable|null $onCountTotalRecords
     * @return $this
     */
    public function setOnCountTotalRecords($onCountTotalRecords);

    /**
     * @param string $filterString
     */
    public function setFilterString($filterString);

    /**
     * @param boolean $filtered
     */
    public function setFiltered($filtered);

    /**
     * @param callable|null $onFilter
     */
    public function setOnFilter($onFilter);

    /**
     * @param $firstResult
     * @return void
     */
    public function setFirstResult($firstResult);

    /**
     * @param $maxResults
     * @return void
     */
    public function setMaxResults($maxResults);


}