<?php

namespace Westwerk\DataTables\Core\DataCollection;

use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;

/**
 * Class AbstractDataCollection
 *
 * @package Westwerk\DataTablesBundle\Core\DataCollection\AbstractDataCollection
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
abstract class AbstractDataCollection implements DataCollectionInterface {

    /**
     * @var ColumnBuilderInterface
     */
    protected $columnBuilder = null;

    public function __construct(ColumnBuilderInterface $columnBuilder) {
        $this->setColumnBuilder($columnBuilder);
    }

    /**
     * @return array|void
     */
    public function toArray() {
        $data = array();
        while (($row = $this->getNext()) && $row !== false) {
            $data[] = $row->toArray();
        }
        return $data;
    }

    /**
     * @return ColumnBuilderInterface
     */
    public function getColumnBuilder()
    {
        return $this->columnBuilder;
    }

    /**
     * @param ColumnBuilderInterface $columnBuilder
     */
    public function setColumnBuilder($columnBuilder)
    {
        $this->columnBuilder = $columnBuilder;
    }



}