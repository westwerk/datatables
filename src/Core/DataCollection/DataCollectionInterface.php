<?php

namespace Westwerk\DataTables\Core\DataCollection;
use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;
use Westwerk\DataTables\Core\Data\DataRowInterface;

/**
 * Class DataCollectionInterface
 *
 * @package Westwerk\DataTablesBundle\Core\DataCollectionInterface
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
interface DataCollectionInterface {

    /**
     * @return DataRowInterface
     */
    public function getNext();

    /**
     * @return void
     */
    public function reset();

    /**
     * @return array
     */
    public function toArray();

    /**
     * @return ColumnBuilderInterface
     */
    public function getColumnBuilder();

}