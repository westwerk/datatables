<?php

namespace Westwerk\DataTables\Core\DataTable;

use Westwerk\DataTables\Core\Column\ColumnInterface;
use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilder;


/**
 * Class AbstractDataTable
 *
 * @package Westwerk\DataTablesBundle\Core\DataTable
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
abstract class AbstractDataTable implements DataTableInterface
{

    /**
     * @var String
     */
    protected $id = null;

    /**
     * @var int
     */
    protected static $instances = 0;


    /**
     * @var ColumnBuilder
     */
    protected $columnBuilder = null;

    /**
     * @var
     */
    protected $dataAggregator = null;

    /**
     * @var array
     */
    protected $options = array();

    /**
     *
     */
    public function __construct()
    {
        //Set initial id
        $className = get_class($this);
        $className = preg_replace('/^.*\\\\/is', '', $className);
        $this->setId(strtolower($className) . '_' . self::$instances);
        self::$instances++;

        //Setup callbacks
        $this->setupDataTable();
        $this->setupColumnBuilder();
        $this->setupColumns($this->getColumnBuilder());
    }


    /**
     * @return void
     */
    public function setupDataTable()
    {

    }

    /**
     * @return void
     */
    public function setupColumnBuilder()
    {
        $this->columnBuilder = new ColumnBuilder();
    }


    /**
     * getColumns
     *
     * @return ColumnInterface[]
     */
    public function getColumns()
    {
        return $this->getColumnBuilder()->getColumns();
    }

    /**
     * getVisibleColumns
     *
     * @return ColumnInterface[]
     */
    public function getVisibleColumns()
    {
        return $this->getColumnBuilder()->getVisibleColumns();
    }

    /**
     * @return ColumnBuilder
     */
    protected function getColumnBuilder()
    {
        return $this->columnBuilder;
    }

    /**
     * @param array $options
     *
     * @return mixed
     */
    abstract function render(array $options = array());

    abstract function processExportRequest();

    abstract function processRequest();

    /**
     * @return mixed
     */
    public function getDataAggregator()
    {
        return $this->dataAggregator;
    }

    /**
     * @param mixed $dataAggregator
     */
    public function setDataAggregator($dataAggregator)
    {
        $this->dataAggregator = $dataAggregator;
    }


    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options)
    {
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            $callable = array($this, $method);
            if (is_callable($callable)) {
                call_user_func($callable, $value);
            } else {
                $this->options[$key] = $value;
            }
        }
        return $this;
    }

    /**
     * @return String
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param String $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}