<?php

namespace Westwerk\DataTables\Core\DataTable;

use Symfony\Component\HttpFoundation\Response;
use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;
use Westwerk\DataTables\Core\Column\ColumnInterface;
use Westwerk\DataTables\Core\DataAggregator\DataAggregatorInterface;
use Westwerk\DataTables\Core\DataSource\DataSourceInterface;

/**
 * Class DataTableInterface
 *
 * @package Westwerk\DataTablesBundle\Core\DataTable
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
interface DataTableInterface
{

    /**
     * @return void
     */
    public function setupDataTable();

    /**
     * @return void
     */
    public function setupColumnBuilder();

    /**
     * @param DataSourceInterface $dataSource
     * @return mixed
     */
    public function setupDataSource(DataSourceInterface $dataSource);

    /**
     * @param ColumnBuilderInterface $columnBuilder
     */
    public function setupColumns(ColumnBuilderInterface $columnBuilder);

    /**
     * getColumns
     *
     * @return ColumnInterface[]
     */
    public function getColumns();

    /**
     * getVisibleColumns
     *
     * @return ColumnInterface[]
     */
    public function getVisibleColumns();


    /**
     * @return mixed
     */
    public function render();

    /**
     * Processes an XML HTTP Request and returns a
     * Response object with a JSON string containing
     * the prepared table data
     *
     * @return Response
     */
    public function processRequest();

    /**
     * @param $dataUri string
     *
     * @return $this
     */
    public function setDataUri($dataUri);


}