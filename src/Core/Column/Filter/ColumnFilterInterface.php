<?php


namespace Westwerk\DataTables\Core\Column\Filter;

use Westwerk\DataTables\Core\Column\ColumnInterface;

interface ColumnFilterInterface {

    /**
     * @return boolean
     */
    public function isEnabled();

    /**
     * @param ColumnInterface $column
     */
    public function setColumn(ColumnInterface $column);

}