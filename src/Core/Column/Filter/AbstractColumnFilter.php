<?php

namespace Westwerk\DataTables\Core\Column\Filter;

use Westwerk\DataTables\Core\Column\ColumnInterface;

abstract class AbstractColumnFilter implements ColumnFilterInterface {



    /**
     * @var array
     */
    protected $options;

    /**
     * @var bool
     */
    protected $enabled = true;

    /**
     * @var ColumnInterface
     */
    protected $column = null;



    /**
     * ColumnFilter constructor
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->setOptions(array_merge($this->getDefaultOptions(), $options));
    }

    /**
     * You may overwrite this in your implementation.
     *
     * @return array
     */
    protected function getDefaultOptions()
    {
        return [];
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return null
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param ColumnInterface $column
     */
    public function setColumn(ColumnInterface $column)
    {
        $this->column = $column;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions($options)
    {
        foreach ($options as $key => $value) {
            switch ($value) {
                case null:
                    $prefix = 'clear';
                    break;
                default:
                    $prefix = 'set';
                    break;
            }
            $method   = $prefix . ucfirst($key);
            $callable = array($this, $method);
            if (is_callable($callable)) {
                call_user_func($callable, $value);
            } else {
                $this->options[$key] = $value;
            }
        }
        return $this;
    }

}