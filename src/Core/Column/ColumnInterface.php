<?php

namespace Westwerk\DataTables\Core\Column;

use Westwerk\DataTables\Core\Column\Filter\ColumnFilterInterface;

/**
 * Class ColumnInterface
 *
 * @package Westwerk\DataTablesBundle\Core\Column
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
interface ColumnInterface
{

    const SORT_DIRECTION_ASC = 1;
    const SORT_DIRECTION_DESC = 2;

    const FILTER_VALUE_NULLABLE = '__NULL__';

    const EXPORT_TYPE_STRING = 1;
    const EXPORT_TYPE_INTEGER = 2;
    const EXPORT_TYPE_FLOAT = 3;

    /**
     * @return string Name
     */
    public function getName();

    /**
     * Get column label.
     *
     * @return string
     */
    public function getLabel();

    /**
     * @return string
     */
    public function getIdentifier();

    /**
     * @return callable|null
     */
    public function getOnGetValue();

    /**
     * @return callable|null
     */
    public function getOnGetExportValue();

    /**
     * @return callable|null
     */
    public function getOnSort();

    /**
     * @return callable|null
     */
    public function getOnFilter();

    /**
     * @return boolean
     */
    public function isFiltered();

    /**
     * @return boolean
     */
    public function isSortable();

    /**
     * @return boolean
     */
    public function isExportable();

    /**
     * @return boolean
     */
    public function isSorted();

    /**
     * @return string
     */
    public function getSortDirection();

    /**
     * @param $sorted
     */
    public function setSorted($sorted);

    /**
     * @param string $sort_direction
     */
    public function setSortDirection($sort_direction);

    /**
     * @param string $filterString
     */
    public function setFilterString($filterString);

    /**
     * @return string
     */
    public function getFilterString();

    /**
     * @return array
     */
    public function getOptions();

    /**
     * @return string
     */
    public function getSource();

    /**
     * @return ColumnFilterInterface
     */
    public function getFilter();

    /**
     * @param null|ColumnFilterInterface $filter
     */
    public function setFilter($filter);

    /**
     * @return bool
     */
    public function hasFilter();

    /**
     * @return boolean
     */
    public function isNullFilterable();

    /**
     * @return boolean
     */
    public function isVirtual();

    /**
     * @return bool
     */
    public function isHidden();

    /**
     * @return int
     */
    public function getExportType();

}