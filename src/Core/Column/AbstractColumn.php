<?php

namespace Westwerk\DataTables\Core\Column;

use Westwerk\DataTables\Core\Column\Filter\ColumnFilterInterface;
use Westwerk\DataTablesBundle\QueryBuilder\Column\Filter\TextColumnFilter;

/**
 * Class AbstractColumn
 *
 * @package Westwerk\DataTablesBundle\Core\Column
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
abstract class AbstractColumn implements ColumnInterface
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var string
     */
    protected $identifier;


    /**
     * Column options.
     *
     * @var array
     */
    protected $options = array();

    /**
     * @var ColumnFilterInterface
     */
    protected $filter = null;

    /**
     * @var string
     */
    protected $filterString = '';

    /**
     * @var bool
     */
    protected $nullFilterable = false;


    /**
     * @var boolean
     */
    protected $exportable = true;

    /**
     * @var integer
     */
    protected $exportType = self::EXPORT_TYPE_STRING;

    /**
     * @var bool
     */
    protected $hidden = false;

    /**
     * @var boolean
     */
    protected $sortable = true;

    /**
     * @var bool
     */
    protected $virtual = false;

    /**
     * @var string
     */
    protected $source;

    /**
     * @var bool
     */
    protected $filtered = false;

    /**
     * @var bool
     */
    protected $sorted = false;

    /**
     * @var string
     */
    protected $sort_direction;


    /**
     * @var null|callable
     */
    protected $onSort = null;

    /**
     * @var null|callable
     */
    protected $onFilter = null;

    /**
     * @var null|callable
     */
    protected $onGetValue = null;

    /**
     * @var null|callable
     */
    protected $onGetExportValue = null;

    /**
     * @param       $name
     * @param array $options
     */
    public function __construct($name, array $options = [])
    {
        $this->setName($name);
        $this->setOptions(array_merge($this->getDefaultOptions(), $options));
    }

    /**
     * @return array
     */
    protected function getDefaultOptions()
    {
        return [
            'filter' => new TextColumnFilter()
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSortable()
    {
        return $this->sortable;
    }

    /**
     * @param $sortable
     *
     * @return $this
     */
    public function setSortable($sortable)
    {
        $this->sortable = $sortable;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isExportable()
    {
        return $this->exportable;
    }

    /**
     * @param boolean $exportable
     */
    public function setExportable($exportable)
    {
        $this->exportable = $exportable;
    }


    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions($options)
    {
        foreach ($options as $key => $value) {
            if ($value === null) {
                $prefix = 'clear';
            }
            else {
                $prefix = 'set';
            }
            $method   = $prefix . ucfirst($key);
            $callable = array($this, $method);
            if (is_callable($callable)) {
                call_user_func($callable, $value);
            } else {
                $this->options[$key] = $value;
            }
        }
        return $this;
    }

    /**
     * @return callable|null
     */
    public function getOnFilter()
    {
        return $this->onFilter;
    }

    /**
     * @param callable|null $onFilter
     */
    public function setOnFilter($onFilter)
    {
        $this->onFilter = $onFilter;
    }

    /**
     * @return callable|null
     */
    public function getOnGetValue()
    {
        return $this->onGetValue;
    }

    /**
     * @param callable|null $onGetValue
     */
    public function setOnGetValue($onGetValue)
    {
        $this->onGetValue = $onGetValue;
    }

    /**
     * @return callable|null
     */
    public function getOnGetExportValue()
    {
        return $this->onGetExportValue;
    }

    /**
     * @param callable|null $onGetExportValue
     */
    public function setOnGetExportValue($onGetExportValue)
    {
        $this->onGetExportValue = $onGetExportValue;
    }


    /**
     * @return callable|null
     */
    public function getOnSort()
    {
        return $this->onSort;
    }

    /**
     * @param callable|null $onSort
     */
    public function setOnSort($onSort)
    {
        $this->onSort = $onSort;
    }

    /**
     * @return boolean
     */
    public function isFiltered()
    {
        return $this->filtered;
    }

    /**
     * @param boolean $filtered
     */
    protected function setFiltered($filtered)
    {
        $this->filtered = $filtered;
    }

    /**
     * @return boolean
     */
    public function isSorted()
    {
        return $this->sorted;
    }

    /**
     * @param $sorted
     */
    public function setSorted($sorted)
    {
        $this->sorted = $sorted;
    }

    /**
     * @return string
     */
    public function getSortDirection()
    {
        return $this->sort_direction;
    }

    /**
     * @param string $sort_direction
     */
    public function setSortDirection($sort_direction)
    {
        $this->sort_direction = $sort_direction;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        $identifier = $this->identifier;
        if (is_null($identifier)) {
            $identifier = $this->getName();
        }
        return preg_replace('/[^_\.a-z90-9]/is', '_', $identifier);
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return ColumnFilterInterface
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @param null|ColumnFilterInterface $filter
     */
    public function setFilter($filter)
    {
        $filter->setColumn($this);
        $this->filter = $filter;
    }

    /**
     * @return bool
     */
    public function hasFilter()
    {
        return ($this->filter instanceof ColumnFilterInterface);
    }

    /**
     * @return $this
     */
    public function clearFilter()
    {
        $this->filter = null;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isNullFilterable()
    {
        return $this->nullFilterable;
    }

    /**
     * @param boolean $nullFilterable
     */
    public function setNullFilterable($nullFilterable)
    {
        $this->nullFilterable = $nullFilterable;
    }

    /**
     * @return string
     */
    public function getFilterString()
    {
        return $this->filterString;
    }

    /**
     * @param string $filterString
     */
    public function setFilterString($filterString)
    {
        $this->setFiltered((strlen($filterString) > 0));
        $this->filterString = $filterString;
    }

    /**
     * @return boolean
     */
    public function isVirtual()
    {
        return $this->virtual;
    }

    /**
     * @param boolean $virtual
     */
    public function setVirtual($virtual)
    {
        $this->virtual = $virtual;
    }

    /**
     * @return boolean
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * @param boolean $hidden
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @return int
     */
    public function getExportType()
    {
        return $this->exportType;
    }

    /**
     * @param int $exportType
     */
    public function setExportType($exportType)
    {
        $this->exportType = $exportType;
    }

}